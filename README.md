# Options in C

Everyone knows and loves `option`s in other, primarily functional languages.
This small, header-only C library brings them to C.

Basic usage:

To create an option to a stack variable

```c
int x = 23;
struct option *op = option_new(&x);
```

To create an option to a good ole pointer

```c
char *s = (char *) malloc(sizeof(char) * 10);
struct option *op = option_new(s);
```

Once we have a `struct option` we have a couple of options on what to do with
it:

We can check if the `option` is some or none with `option_is_some` and it's
obverse `option_is_none`

To call a function on the inner value (*iff* it's `some`):

```c
void print_string(char *s) {
    printf("inner value is %s\n", s);
}

// ...
char *s = (char *) malloc(sizeof(char) * 10);
struct option *op = option_new(s);
option_with(string_option, print_string);
```

To call a function on the `option` itself, there's `option_and_then`.

Once we're done with the option, we can free it in one of three ways, to free with
`option` and only the `option` use `option_free`; to free the `option` and the
inner, wrapped pointer, use `option_free_deep`; finally, to free the `option`
and the inner pointer with custom logic, use `option_free_with`.
