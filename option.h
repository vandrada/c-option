// option.h
//
// Simple header-only library to work with option types in C. Has functions for
// creating, freeing, and interacting with options in different ways. At it's
// highest level, options are created by passing in a pointer, unwrapping one
// returns the raw pointer, unwrapping an empty option exits immeditaly, and a
// handful of comobinators are included to act on the inner pointer, apply
// functions to the option, and map an option to a new option.
//
// To keep the option in a consistent state, you should interfere with it's
// members directly, instead use `option_set` to change it's value.
//
// option_new(inner): creates a new option
// option_free(op): frees an option
// option_free_deep(op)
// option_free_with(op,f)
// option_unwrap(op): gets the contents of an option
// option_is_some(op): returns true iff the option is valid
// option_is_none(op): returns true iff the option is not valid
// option_set(op, inner): updates the contents of an option
//
// option_and_then(op,f): applies `f` to the option `op`
// option_with(op,f): applies `f` to the contents of `op`
// option_from(op,f): creates a new option by applying `f` to `op`

enum option_type {
    some,
    none
};

// optional type for generic pointers
// since void* is the closest C has to generics, it's up to you to cast it to
// the right type.
// Don't use the fields in this struct directly, instead use the accessors
// below.
struct option {
    enum option_type _type;
    void *_inner;
};

#define __option_new(inner,type) ({\
    struct option *o = malloc(sizeof(struct option));\
    o->_inner = (inner);\
    o->_type = (type);\
    o;\
})

#define __option_type(inner) ({\
    enum option_type type;\
    if ((inner) == NULL) {\
        type = none;\
    } else {\
        type = some;\
    }\
    type;\
})

// Creates a new option that will hold the pointer `inner`. It's underlying type
// (either none or some) will be set accordingly
//
//  option_new(NULL) -> struct option op { inner = NULL, type = none }
//  option_new(s)    -> struct option op { inner = s, type = some }
#define option_new(inner) ({\
    enum option_type type = __option_type((inner));\
    __option_new((inner), type);\
})

// Frees the memory that was allocated for the option type, the internal pointer
// is not touched and that is left to you to free.
#define option_free(op) free((op))

// frees the contents of option `op` by calling `f` on the inner pointer first.
// `f` can be as complex as need be.
#define option_free_with(op,f) ({\
    f((op)->_inner);\
    option_free((op));\
})

// frees the inner pointer and the option itself. `free` is used for the inner
// pointer. If something more complex is needed, used `option_free_with`
#define option_free_deep(op) option_free_with((op),free)

// Unwraps the option and returns the inner pointer. If the pointer was NULL,
// a message is logged, and the program exits.
#define option_unwrap(op) ({\
    void *retval;\
    if ((op)->_type == none) {\
        fprintf(stdout, "tried to unwrap an option that's none at %s:%d\n", __FILE__, __LINE__);\
        exit(1);\
    } else {\
        retval = (op)->_inner;\
    }\
    retval;\
})

// Simple test to determine if the option is valid
#define option_is_some(op) (op)->_type == some

// Simple test to determine if the option is not valid
#define option_is_none(op) !option_is_some((op))

// updates the pointer that the option type holds; again, the type is derived
// from the pointer
#define option_set(op, inner) ({\
    enum option_type type = __option_type(inner);\
    (op)->_inner = inner;\
    (op)->_type = type;\
    (op);\
})

// simple combinators that remove the boiler plate code of having to use `if`s
// everywhere

// calls f on op if and only if it's some
// `f` is called on the option itself so the signature of f should be something
// along the lines of
//  <return type> f(struct option *op)
#define option_and_then(op,f) ({\
    if (option_is_some((op))) {\
        f((op));\
    }\
})

// calls f on the contents of op if and only if it's some
// f is called on the inner pointer
#define option_with(op,f) ({\
    if (option_is_some((op))) {\
        f(option_unwrap((op)));\
    }\
})

// creates a new option by applying a function to this option
#define option_from(op,f) ({\
    void *inner = option_unwrap(op);\
    void *result = f(inner);\
    option_new(result);\
})
